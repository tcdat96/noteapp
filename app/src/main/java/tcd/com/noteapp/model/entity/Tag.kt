package tcd.com.noteapp.model.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity
@Parcelize
data class Tag(
        @PrimaryKey(autoGenerate = true) var id: Int,
        @ColumnInfo(name = "name") var name: String,
        @ColumnInfo(name = "lastModified") var lastModified: Long = 0) : Parcelable {

    companion object {
        const val DEFAULT_ID = 0
    }

    constructor(name: String) : this(DEFAULT_ID, name)

    override fun equals(other: Any?): Boolean {
        return (other as? Tag)?.let { that ->
            id == that.id && name == that.name && lastModified == that.lastModified
        } ?: false
    }

    override fun hashCode(): Int {
        var result = id
        result = 31 * result + name.hashCode()
        result = 31 * result + lastModified.hashCode()
        return result
    }
}