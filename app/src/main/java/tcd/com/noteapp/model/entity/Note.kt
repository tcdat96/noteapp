package tcd.com.noteapp.model.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Entity
@Parcelize
data class Note(
        @PrimaryKey(autoGenerate = true) var id: Int,
        @ColumnInfo(name = "title") var title: String,
        @ColumnInfo(name = "content") var content: String,
        @ColumnInfo(name = "tagIds") var tagIds: ArrayList<Int>,
        @ColumnInfo(name = "lastModified") var lastModified: Long) : Parcelable {

    companion object {
        const val DEFAULT_ID = 0
    }

    // for state checking in adapter
    @IgnoredOnParcel
    var isIncluded = false

    constructor() : this(DEFAULT_ID, "", "", arrayListOf(), 0)

    override fun equals(other: Any?): Boolean {
        return (other as? Note)?.let { that ->
            id == that.id
                    && title == that.title
                    && content == that.content
                    && tagIds.size == that.tagIds.size && tagIds.containsAll(that.tagIds)
                    && lastModified == that.lastModified
        } ?: false
    }

    override fun hashCode(): Int {
        var result = id
        result = 31 * result + title.hashCode()
        result = 31 * result + content.hashCode()
        result = 31 * result + tagIds.hashCode()
        result = 31 * result + lastModified.hashCode()
        return result
    }

//    override fun toString(): String {
//        return "Note($id, $title, $content, $tagIds)"
//    }
}