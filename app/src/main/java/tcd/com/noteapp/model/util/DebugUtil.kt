package tcd.com.noteapp.model.util

import tcd.com.noteapp.model.entity.Note
import tcd.com.noteapp.model.entity.Tag
import java.util.*

object DebugUtil {

    private const val NUMBER_OF_NOTES = 100
    private const val NUMBER_OF_TAGS = 10

    @JvmStatic
    fun getNotes(): List<Note> {
        val notes = arrayListOf<Note>()
        val calendar = Calendar.getInstance()
        for (i in 0 until NUMBER_OF_NOTES) {
            val note = Note(i, "note #$i",
                    "#Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book",
                    ArrayList((1..3).map { (0 until NUMBER_OF_TAGS).random() }.distinct()),
                    calendar.timeInMillis
            )
            notes.add(note)
        }
        return notes
    }

    @JvmStatic
    fun getTags(): List<Tag> {
        val tags = arrayListOf<Tag>()
        for (i in 0 until NUMBER_OF_TAGS) {
            val tag = Tag(i, "tag #$i")
            tags.add(tag)
        }
        return tags
    }
}