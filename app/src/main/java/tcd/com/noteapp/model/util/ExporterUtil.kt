package tcd.com.noteapp.model.util

import android.content.Context
import android.net.Uri
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import tcd.com.noteapp.model.entity.Note
import tcd.com.noteapp.model.entity.Tag
import tcd.com.noteapp.model.db.DataTypeConverter
import tcd.com.noteapp.model.util.StorageUtil.getExportedFile
import java.io.FileOutputStream
import java.lang.Exception


object ExporterUtil {

    @JvmStatic
    fun export(context: Context, notes: List<Note>, tags: List<Tag>) {
        try {
            val json = toJson(notes, tags)
            val data = json.toString(0).toByteArray()
            val file = getExportedFile(context)
            FileOutputStream(file).use { stream -> stream.write(data) }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @JvmStatic
    private fun toJson(notes: List<Note>, tags: List<Tag>): JSONObject {
        val json = JSONObject()

        val tagArr = JSONArray()
        for (i in 0 until tags.size) {
            val tag = tags[i]
            val tagJson = JSONObject()
            tagJson.put("id", tag.id)
            tagJson.put("name", tag.name)
            tagArr.put(i, tagJson)
        }
        json.put("tags", tagArr)

        val noteArr = JSONArray()
        val converter = DataTypeConverter()
        for (i in 0 until notes.size) {
            val note = notes[i]
            val noteJson = JSONObject()
            noteJson.put("id", note.id)
            noteJson.put("title", note.title)
            noteJson.put("content", note.content)
            noteJson.put("tagIds", converter.toString(note.tagIds))
            noteJson.put("lastModified", note.lastModified)
            noteArr.put(i, noteJson)
        }
        json.put("notes", noteArr)

        return json
    }

    @JvmStatic
    fun import(context: Context, uri: Uri): Pair<List<Note>, List<Tag>>? {
        val data = StorageUtil.readData(context, uri)
        return try {
            val json = JSONObject(data)
            Pair(
                    getNotesFromJson(json),
                    getTagsFromJson(json)
            )
        } catch (e: JSONException) { null }
    }

    @JvmStatic
    private fun getTagsFromJson(json: JSONObject): List<Tag> {
        val tags = arrayListOf<Tag>()
        val tagArr = json.optJSONArray("tags")
        for (i in 0 until tagArr.length()) {
            (tagArr[i] as? JSONObject)?.run {
                val tag = Tag(getInt("id"), getString("name"))
                tags.add(tag)
            }
        }
        return tags
    }

    @JvmStatic
    private fun getNotesFromJson(json: JSONObject): List<Note> {
        val notes = arrayListOf<Note>()
        val noteArr = json.optJSONArray("notes")
        val converter = DataTypeConverter()
        for (i in 0 until noteArr.length()) {
            (noteArr[i] as? JSONObject)?.run {
                val id = getInt("id")
                val title = getString("title")
                val content = getString("content")
                val tagIds = converter.toIntList(getString("tagIds"))
                val lastModified = getLong("lastModified")
                val note = Note(id, title, content, tagIds, lastModified)
                notes.add(note)
            }
        }
        return notes
    }
}