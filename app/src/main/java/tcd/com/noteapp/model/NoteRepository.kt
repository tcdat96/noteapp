package tcd.com.noteapp.model

import android.app.Application
import androidx.lifecycle.LiveData
import tcd.com.noteapp.model.db.AppDatabase
import tcd.com.noteapp.model.db.NoteDao
import tcd.com.noteapp.model.db.TagDao
import tcd.com.noteapp.model.entity.Note
import tcd.com.noteapp.model.entity.Tag

class NoteRepository private constructor(application: Application) {
    private var mNoteDao: NoteDao
    private var mTagDao: TagDao

    private var mNotes: LiveData<List<Note>>
    private var mTags: LiveData<List<Tag>>

    companion object {
        @Volatile private var INSTANCE: NoteRepository? = null

        fun getInstance(application: Application): NoteRepository =
                INSTANCE ?: synchronized(this) {
                    INSTANCE ?: NoteRepository(application).also { INSTANCE = it }
                }
    }

    init {
        val database: AppDatabase = AppDatabase.getInstance(application.applicationContext)

        mNoteDao = database.noteDao()
        mTagDao = database.tagDao()

        mNotes = mNoteDao.getAll()
        mTags = mTagDao.getAll()
    }

    // notes
    fun getNotes(): LiveData<List<Note>> {
        return mNotes
    }

    suspend fun insertNote(vararg notes: Note) {
        mNoteDao.insert(*notes)
    }

    suspend fun updateNote(note: Note) {
        mNoteDao.update(note)
    }

    suspend fun deleteNote(id: Int) {
        mNoteDao.delete(id)
    }

    suspend fun deleteAllNotes() {
        mNoteDao.deleteAll()
    }


    // tags
    fun getTags(): LiveData<List<Tag>> {
        return mTags
    }

    suspend fun insertTag(vararg tags: Tag) {
        mTagDao.insert(*tags)
    }

    suspend fun updateTag(tag: Tag) {
        mTagDao.update(tag)
    }

    suspend fun deleteTag(id: Int) {
        mTagDao.delete(id)
    }

    suspend fun deleteAllTags() {
        mTagDao.deleteAll()
    }
}