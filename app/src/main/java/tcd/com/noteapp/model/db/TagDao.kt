package tcd.com.noteapp.model.db

import androidx.lifecycle.LiveData
import androidx.room.*
import tcd.com.noteapp.model.entity.Tag

@Dao
interface TagDao {
    @Query("SELECT * FROM tag")
    fun getAll(): LiveData<List<Tag>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(vararg tags: Tag)

    @Update
    suspend fun update(tag: Tag)

    @Query("DELETE FROM tag WHERE id = :id")
    suspend fun delete(id: Int)

    @Query("DELETE FROM tag")
    suspend fun deleteAll()
}