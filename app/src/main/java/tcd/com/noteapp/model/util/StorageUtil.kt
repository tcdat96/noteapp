package tcd.com.noteapp.model.util

import android.content.Context
import android.net.Uri
import android.os.Environment
import java.io.BufferedReader
import java.io.File
import java.io.IOException
import java.io.InputStreamReader
import java.text.SimpleDateFormat
import java.util.*

object StorageUtil {

    @JvmStatic
    fun getTimeStamp(): String {
        return SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
    }

    @JvmStatic
    fun getExportedFile(context: Context): File? {
        if (!isExternalStorageWritable()) {
            return null
        }
        val file = File(context.getExternalFilesDir(null), getTimeStamp())
        return if (file.createNewFile()) file else null
    }

    @JvmStatic
    fun getLastExternalFile(context: Context): Uri? {
        if (!isExternalStorageWritable()) {
            return null
        }

        val dir = File(context.getExternalFilesDir(null), "")
        val files = dir.listFiles()
        if (files.isEmpty()) {
            return null
        }

        var latest = files[0]
        files.forEach {
            if (latest.lastModified() < it.lastModified()) {
                latest = it
            }
        }
        return Uri.fromFile(latest)
    }

    @JvmStatic
    fun isExternalStorageWritable(): Boolean {
        return Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED
    }

    @JvmStatic
    @Throws(IOException::class)
    fun readData(context: Context, uri: Uri): String {
        val stringBuilder = StringBuilder()
        context.contentResolver.openInputStream(uri)?.use { inputStream ->
            BufferedReader(InputStreamReader(inputStream)).use { reader ->
                var line: String? = reader.readLine()
                while (line != null) {
                    stringBuilder.append(line)
                    line = reader.readLine()
                }
            }
        }
        return stringBuilder.toString()
    }
}