package tcd.com.noteapp.model.db

import androidx.lifecycle.LiveData
import androidx.room.*
import tcd.com.noteapp.model.entity.Note

@Dao
interface NoteDao {
    @Query("SELECT * FROM note")
    fun getAll(): LiveData<List<Note>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(vararg notes: Note)

    @Update
    suspend fun update(note: Note)

    @Query("DELETE FROM note WHERE id = :id")
    suspend fun delete(id: Int)

    @Query("DELETE FROM note")
    suspend fun deleteAll()
}