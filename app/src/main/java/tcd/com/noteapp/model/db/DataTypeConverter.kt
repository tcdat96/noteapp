package tcd.com.noteapp.model.db

import androidx.room.TypeConverter
import java.lang.NumberFormatException

class DataTypeConverter {
    @TypeConverter
    fun toString(strArr : List<Int>) : String {
        return strArr.joinToString(",")
    }

    @TypeConverter
    fun toIntList(string: String) : ArrayList<Int> {
        return ArrayList(string.split(",").mapNotNull {
            try {
                it.toInt()
            } catch (e: NumberFormatException) {
                null
            }
        })
    }
}