package tcd.com.noteapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import tcd.com.noteapp.model.NoteRepository
import tcd.com.noteapp.model.entity.Tag
import java.util.*

class TagViewModel(application: Application) : AndroidViewModel(application) {
    private var repository: NoteRepository = NoteRepository.getInstance(application)

    private var mTags: LiveData<List<Tag>> = repository.getTags()

    fun insert(vararg tags: Tag) = viewModelScope.launch {
        val calendar = Calendar.getInstance()
        for (tag in tags) {
            tag.lastModified = calendar.timeInMillis
        }
        repository.insertTag(*tags)
    }

    fun update(tag: Tag) = viewModelScope.launch {
        tag.lastModified = Calendar.getInstance().timeInMillis
        repository.updateTag(tag)
    }

    fun delete(id: Int) = viewModelScope.launch {
        repository.deleteTag(id)
    }

    fun deleteAll() = viewModelScope.launch {
        repository.deleteAllTags()
    }

    fun getTags() = mTags

    fun getRawTags() = mTags.value
}