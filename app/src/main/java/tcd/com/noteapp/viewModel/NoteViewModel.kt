package tcd.com.noteapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import tcd.com.noteapp.model.entity.Note
import tcd.com.noteapp.model.NoteRepository
import java.util.*

class NoteViewModel(application: Application) : AndroidViewModel(application) {
    private var repository: NoteRepository = NoteRepository.getInstance(application)

    private var mNotes: LiveData<List<Note>> = repository.getNotes()

    fun insert(vararg notes: Note) = viewModelScope.launch {
        val calendar = Calendar.getInstance()
        for (note in notes) {
            note.lastModified = calendar.timeInMillis
        }
        repository.insertNote(*notes)
    }

    fun update(note: Note) = viewModelScope.launch {
        note.lastModified = Calendar.getInstance().timeInMillis
        repository.updateNote(note)
    }

    fun delete(id: Int) = viewModelScope.launch {
        repository.deleteNote(id)
    }

    fun deleteAll() = viewModelScope.launch {
        repository.deleteAllNotes()
    }

    fun getNotes() = mNotes

    fun getRawNotes() = mNotes.value
}