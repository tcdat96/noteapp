package tcd.com.noteapp.ui.view

import android.app.Activity
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.coroutines.*
import tcd.com.noteapp.R
import tcd.com.noteapp.model.entity.Note
import tcd.com.noteapp.model.entity.Tag
import tcd.com.noteapp.model.util.DebugUtil
import tcd.com.noteapp.model.util.ExporterUtil
import tcd.com.noteapp.model.util.StorageUtil
import tcd.com.noteapp.ui.adapter.NoteAdapter
import tcd.com.noteapp.ui.view.EditNoteActivity.Companion.KEY_NOTE
import tcd.com.noteapp.viewModel.NoteViewModel
import tcd.com.noteapp.viewModel.TagViewModel
import kotlin.coroutines.CoroutineContext


class MainActivity : AppCompatActivity(), CoroutineScope {

    companion object {
        private const val REQUEST_CODE_GET_BACKUP_FILE = 1
    }

    private val mJob = Job()
    override val coroutineContext: CoroutineContext
        get() = mJob + Dispatchers.Main

    private lateinit var mTagViewModel: TagViewModel
    private lateinit var mNoteViewModel: NoteViewModel

    private lateinit var mNotesRecyclerView: RecyclerView
    private lateinit var mNoteAdapter: NoteAdapter

    private var mSearchQuery: String? = null
    private lateinit var mProgressBar: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initViewModels()

        initUiComponents()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)

        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        (menu.getItem(0)?.actionView as? SearchView)?.apply {
            setSearchableInfo(searchManager.getSearchableInfo(componentName))
            setIconifiedByDefault(false)

            setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String): Boolean {
                    return false
                }

                override fun onQueryTextChange(query: String): Boolean {
                    mSearchQuery = query
                    mNoteAdapter.filter.filter(query)
                    return false
                }
            })
        }

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.menu_edit_tag -> startActivity(Intent(this, EditTagActivity::class.java))
            R.id.menu_import -> showImportOptions()
            R.id.menu_export -> {
                runIO {
                    val tags = mTagViewModel.getRawTags()
                    val notes = mNoteViewModel.getRawNotes()
                    if (tags != null && notes != null) {
                        ExporterUtil.export(this@MainActivity, notes, tags)
                    }
                }
            }
            R.id.menu_export_dummy -> {
                runIO {
                    ExporterUtil.export(this@MainActivity, DebugUtil.getNotes(), DebugUtil.getTags())
                }
            }
            R.id.menu_delete -> {
                runIO {
                    mNoteViewModel.deleteAll()
                    mTagViewModel.deleteAll()
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun runIO(ioFunc: suspend () -> Unit) {
        mProgressBar.visibility = View.VISIBLE
        launch(Dispatchers.IO) {
            ioFunc()
            withContext(Dispatchers.Main) {
                mProgressBar.visibility = View.GONE
            }
        }
    }

    private fun showImportOptions() {
        val options = arrayOf(
                getString(R.string.restore_latest_backup),
                getString(R.string.choose_backup_file)
        )
        AlertDialog.Builder(this).apply {
            setTitle(getString(R.string.pick_backup_option))
            setItems(options) { _, which ->
                if (which == 0) {
                    val uri = StorageUtil.getLastExternalFile(this@MainActivity)
                    uri?.also { restoreNotes(uri) }
                            ?: Toast.makeText(this@MainActivity, R.string.menu_restore, Toast.LENGTH_SHORT).show()
                } else {
                    val intent = Intent(Intent.ACTION_GET_CONTENT).apply {
                        addCategory(Intent.CATEGORY_OPENABLE)
                        type = "application/octet-stream"
                    }
                    startActivityForResult(intent, REQUEST_CODE_GET_BACKUP_FILE)
                }
            }
        }.show()
    }

    private fun restoreNotes(uri: Uri) {
        runIO {
            val result = ExporterUtil.import(this@MainActivity, uri)
            result?.run {
                mNoteViewModel.insert(*first.toTypedArray())
                mTagViewModel.insert(*second.toTypedArray())
            } ?: withContext(Dispatchers.Main) {
                Toast.makeText(this@MainActivity, R.string.error_restore_backup, Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mJob.cancel()
    }

    private fun initViewModels() {
        mTagViewModel = ViewModelProviders.of(this).get(TagViewModel::class.java)
        mTagViewModel.getTags().observe(this, Observer<List<Tag>?> {
            it?.run { mNoteAdapter.setTags(it) }
        })

        mNoteViewModel = ViewModelProviders.of(this).get(NoteViewModel::class.java)
        mNoteViewModel.getNotes().observe(this, Observer<List<Note>?> { notes ->
            notes?.sortedByDescending { note -> note.lastModified }?.run {
                mNoteAdapter.setNotes(this, mSearchQuery)
                // animation for adding item
                mNotesRecyclerView.layoutManager?.run {
                    onRestoreInstanceState(onSaveInstanceState())
                }
            }
        })
    }

    private fun initUiComponents() {
        initNotesRecyclerView()

        mProgressBar = findViewById(R.id.progress_bar)

        findViewById<FloatingActionButton>(R.id.fab_add_note).setOnClickListener {
            val intent = Intent(this@MainActivity, EditNoteActivity::class.java)
            startActivity(intent)
        }
    }

    private fun initNotesRecyclerView() {
        mNoteAdapter = NoteAdapter().apply {
            mOnItemClickListener = object : NoteAdapter.OnItemClickListener {
                override fun onItemClick(note: Note) {
                    val intent = Intent(this@MainActivity, EditNoteActivity::class.java).apply {
                        putExtra(KEY_NOTE, note)
                    }
                    startActivity(intent)
                }
            }
        }

        mNotesRecyclerView = findViewById(R.id.rv_notes)
        mNotesRecyclerView.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            itemAnimator = DefaultItemAnimator()
            adapter = mNoteAdapter
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_CODE_GET_BACKUP_FILE -> {
                if (resultCode == Activity.RESULT_OK) {
                    data?.data?.run { restoreNotes(this) }
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}
