package tcd.com.noteapp.ui.view

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import tcd.com.noteapp.R
import tcd.com.noteapp.model.entity.Tag
import tcd.com.noteapp.ui.adapter.SelectTagAdapter
import tcd.com.noteapp.viewModel.TagViewModel

class SelectTagActivity : AppCompatActivity() {

    companion object {
        const val KEY_SELECTED_TAGS = "keySelectedTags"
    }

    private lateinit var mTagViewModel: TagViewModel

    private lateinit var mSelectTagAdapter : SelectTagAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_tag)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val selectedTags = intent?.extras?.getIntegerArrayList(KEY_SELECTED_TAGS) ?: listOf<Int>()
        initTagListRecyclerView(selectedTags)

        initViewModels()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        val intent = Intent().apply {
            putExtra(KEY_SELECTED_TAGS, mSelectTagAdapter.getSelectedTag())
        }
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    private fun initTagListRecyclerView(selectedTags: List<Int>) {
        val tagListRv = findViewById<RecyclerView>(R.id.rv_tags)
        tagListRv.layoutManager = LinearLayoutManager(this)
        tagListRv.itemAnimator = DefaultItemAnimator()

        mSelectTagAdapter = SelectTagAdapter(HashSet(selectedTags))
        tagListRv.adapter = mSelectTagAdapter
    }

    private fun initViewModels() {
        mTagViewModel = ViewModelProviders.of(this).get(TagViewModel::class.java)
        mTagViewModel.getTags().observe(this, Observer<List<Tag>?> {
            it?.run {
                mSelectTagAdapter.setTags(this)
            }
        })
    }
}
