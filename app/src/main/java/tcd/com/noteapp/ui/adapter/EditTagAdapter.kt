package tcd.com.noteapp.ui.adapter

import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import tcd.com.noteapp.R
import tcd.com.noteapp.model.entity.Tag
import tcd.com.noteapp.ui.adapter.callback.TagDiffCallback


class EditTagAdapter : RecyclerView.Adapter<EditTagAdapter.TagViewHolder>() {

    private var mTags = arrayListOf<Tag>()

    var mOnTagModifiedListener: OnTagModifiedListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TagViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.row_edit_tag_item, parent, false)
        return TagViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return mTags.size
    }

    override fun onBindViewHolder(holder: TagViewHolder, position: Int) {
        val tag = mTags[position]
        holder.nameEditText.text = tag.name
    }

    fun setTags(newTags: List<Tag>) {
        val diffCallback = TagDiffCallback(mTags, newTags)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        mTags.clear()
        mTags.addAll(newTags)
        diffResult.dispatchUpdatesTo(this)
    }

    inner class TagViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var nameEditText = itemView.findViewById<TextView>(R.id.et_title)!!
        private var deleteImageButton = itemView.findViewById<ImageButton>(R.id.ib_delete)!!

        init {
            nameEditText.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                    s?.toString()?.run {
                        val tag = mTags[adapterPosition]
                        if (isNotBlank() && tag.name != this) {
                            tag.name = this
                            mOnTagModifiedListener?.onUpdate(tag)
                        }
                    }
                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            })

            deleteImageButton.setOnClickListener {
                mOnTagModifiedListener?.onDelete(mTags[adapterPosition])
            }
        }
    }

    interface OnTagModifiedListener {
        fun onUpdate(tag: Tag)
        fun onDelete(tag: Tag)
    }
}