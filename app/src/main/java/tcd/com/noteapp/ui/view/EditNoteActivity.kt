package tcd.com.noteapp.ui.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.flexbox.FlexboxLayout
import tcd.com.noteapp.R
import tcd.com.noteapp.model.entity.Note
import tcd.com.noteapp.model.entity.Tag
import tcd.com.noteapp.viewModel.NoteViewModel
import tcd.com.noteapp.viewModel.TagViewModel

class EditNoteActivity : AppCompatActivity() {

    companion object {
        const val KEY_NOTE = "keyNote"
        const val REQUEST_CODE_ADD_TAG = 1
    }

    private var mNote : Note = Note()

    private lateinit var mTagViewModel: TagViewModel
    private lateinit var mNoteViewModel: NoteViewModel

    private lateinit var mTitleEditText: EditText
    private lateinit var mContentEditText: EditText
    private lateinit var mTagsContainer: FlexboxLayout

    private val mOnTagClickListener = View.OnClickListener {
        val intent = Intent(this@EditNoteActivity, SelectTagActivity::class.java).apply {
            putExtra(SelectTagActivity.KEY_SELECTED_TAGS, mNote.tagIds)
        }
        startActivityForResult(intent, REQUEST_CODE_ADD_TAG)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_note)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        mNote.title = getString(R.string.default_note_title)
        intent?.getParcelableExtra<Note>(KEY_NOTE)?.run {
            mNote = this
        }

        initViewModels()
        initUiComponents()
    }

    private fun initUiComponents() {
        mTitleEditText = findViewById(R.id.et_note_title)
        mTitleEditText.append(mNote.title)

        mContentEditText = findViewById(R.id.et_note_content)
        mContentEditText.append(mNote.content)

        mTagsContainer = findViewById(R.id.fl_tags_container)

        findViewById<TextView>(R.id.btn_add_tag).setOnClickListener(mOnTagClickListener)

        populateTagsContainer()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_edit_note, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            R.id.menu_save -> {
                saveNote()
                finish()
            }
            R.id.menu_delete -> {
                if (mNote.id != Note.DEFAULT_ID) {
                    mNoteViewModel.delete(mNote.id)
                }
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initViewModels() {
        mTagViewModel = ViewModelProviders.of(this).get(TagViewModel::class.java)
        mTagViewModel.getTags().observe(this, Observer<List<Tag>?> {
            it?.run { populateTagsContainer() }
        })

        mNoteViewModel = ViewModelProviders.of(this).get(NoteViewModel::class.java)
    }

    override fun onBackPressed() {
        saveNote()
        super.onBackPressed()
    }

    fun saveNote() {
        mTitleEditText.text?.toString()?.run {
            mNote.title = if (isNotBlank()) this else getString(R.string.default_note_title)
        }
        mContentEditText.text?.toString()?.run {
            mNote.content = this
        }
        if (mNote.id != Note.DEFAULT_ID) {
            mNoteViewModel.update(mNote)
        } else {
            mNoteViewModel.insert(mNote)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_CODE_ADD_TAG -> {
                if (resultCode == Activity.RESULT_OK) {
                    data?.getIntegerArrayListExtra(SelectTagActivity.KEY_SELECTED_TAGS)?.run {
                        mNote.tagIds = this
                        populateTagsContainer()
                    }
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun populateTagsContainer() {
        val tags = mTagViewModel.getRawTags()?.map { it.id to it }?.toMap()
        if (tags.isNullOrEmpty()) {
            return
        }

        mTagsContainer.removeViews(0, mTagsContainer.childCount - 1)
        for (tagId in mNote.tagIds) {
            tags[tagId]?.run {
                val tagView = layoutInflater.inflate(R.layout.widget_tag_view, mTagsContainer, false)
                        as TextView
                tagView.text = name
                tagView.setOnClickListener(mOnTagClickListener)
                mTagsContainer.addView(tagView, mTagsContainer.childCount - 1)
            }
        }
    }
}
