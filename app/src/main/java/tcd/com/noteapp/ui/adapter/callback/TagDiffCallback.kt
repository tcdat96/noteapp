package tcd.com.noteapp.ui.adapter.callback

import androidx.recyclerview.widget.DiffUtil
import tcd.com.noteapp.model.entity.Tag

class TagDiffCallback(private val oldList: List<Tag>, private val newList: List<Tag>) : DiffUtil.Callback() {

    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] == newList[newItemPosition]
    }
}