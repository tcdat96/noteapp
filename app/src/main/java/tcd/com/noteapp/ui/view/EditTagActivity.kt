package tcd.com.noteapp.ui.view

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.ImageButton
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import tcd.com.noteapp.R
import tcd.com.noteapp.model.entity.Tag
import tcd.com.noteapp.ui.adapter.EditTagAdapter
import tcd.com.noteapp.viewModel.TagViewModel

class EditTagActivity : AppCompatActivity() {

    private lateinit var mTagViewModel: TagViewModel

    private lateinit var mTagsRecyclerView: RecyclerView
    private lateinit var mTagAdapter: EditTagAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_tag)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        setTitle(R.string.menu_edit_tags)

        initUiComponents()
        initViewModels()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initUiComponents() {
        val newTagEditText = findViewById<EditText>(R.id.et_tag_name)
        val addImageButton = findViewById<ImageButton>(R.id.ib_add_tag)

        newTagEditText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun afterTextChanged(s: Editable?) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                s?.toString()?.run {
                    addImageButton.visibility = if (isNotBlank()) View.VISIBLE else View.INVISIBLE
                }
            }
        })

        fun addNewTag() {
            val newTag = Tag(newTagEditText.text.toString())
            newTagEditText.text.clear()
            newTagEditText.requestFocus()
            mTagViewModel.insert(newTag)
        }

        newTagEditText.setOnKeyListener { _: View, keyCode: Int, event: KeyEvent ->
            if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                addNewTag()
                return@setOnKeyListener true
            }
            false
        }

        addImageButton.setOnClickListener {
            addNewTag()
        }

        initTagListRecyclerView()
    }

    private fun initTagListRecyclerView() {
        mTagsRecyclerView = findViewById(R.id.rv_tags)
        mTagsRecyclerView.layoutManager = LinearLayoutManager(this)
        mTagsRecyclerView.itemAnimator = DefaultItemAnimator()

        mTagAdapter = EditTagAdapter()
        mTagAdapter.mOnTagModifiedListener = object : EditTagAdapter.OnTagModifiedListener {
            override fun onUpdate(tag: Tag) {
                mTagViewModel.update(tag)
            }

            override fun onDelete(tag: Tag) {
                mTagViewModel.delete(tag.id)
            }
        }
        mTagsRecyclerView.adapter = mTagAdapter
    }

    private fun initViewModels() {
        mTagViewModel = ViewModelProviders.of(this).get(TagViewModel::class.java)
        mTagViewModel.getTags().observe(this, Observer<List<Tag>?> { tags ->
            tags?.sortedByDescending { tag -> tag.lastModified }?.run {
                mTagAdapter.setTags(this)
                // animation for adding item
                mTagsRecyclerView.layoutManager?.run {
                    onRestoreInstanceState(onSaveInstanceState())
                }
            }
        })
    }
}