package tcd.com.noteapp.ui.adapter

import android.widget.Filter
import tcd.com.noteapp.model.entity.Note
import tcd.com.noteapp.model.entity.Tag

class NoteFilter(val mAdapter: NoteAdapter, val mNotes: List<Note>, val mTags: List<Tag>) {
    private val mFilter: BaseFilter
    private val mFilteringNotes = arrayListOf<Note>()

    init {
        val filters = listOf(
                TitleFilter(),
                TagFilter(),
                ContentFilter()
        )
        for (i in 0 until filters.size - 1) {
            filters[i].nextFilter = filters[i + 1]
        }
        mFilter = filters[0]
    }

    fun getFilter(): Filter {
        mFilteringNotes.forEach { it.isIncluded = false }
        mFilteringNotes.clear()
        return mFilter
    }

    private abstract inner class BaseFilter : Filter() {
        var nextFilter: Filter? = null

        abstract fun getFilterResult(constraint: CharSequence): List<Note>

        override fun performFiltering(constraint: CharSequence?): FilterResults {
            val query = constraint.toString()
            val filterResults = FilterResults()
            val notes = if (query.isEmpty()) mNotes else getFilterResult(query)
            notes.forEach { it.isIncluded = true }
            filterResults.values = notes
            return filterResults
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            val notes = results?.values as ArrayList<Note>
            mFilteringNotes.addAll(notes)
            mAdapter.setNotesInternal(mFilteringNotes)
            if (notes != mNotes) {
                nextFilter?.filter(constraint)
            }
        }
    }

    private inner class TitleFilter : BaseFilter() {
        override fun getFilterResult(constraint: CharSequence): List<Note> {
            return mNotes.filter { !it.isIncluded && it.title.contains(constraint) }
        }
    }

    private inner class TagFilter : BaseFilter() {
        override fun getFilterResult(constraint: CharSequence): List<Note> {
            val tagIds = mTags
                    .filter { it.name.contains(constraint) }
                    .map { it.id }
            return mNotes.filter { note ->
                !note.isIncluded && note.tagIds.any { it in tagIds }
            }
        }
    }

    private inner class ContentFilter : BaseFilter() {
        override fun getFilterResult(constraint: CharSequence): List<Note> {
            return mNotes.filter { !it.isIncluded && it.content.contains(constraint) }
        }
    }
}