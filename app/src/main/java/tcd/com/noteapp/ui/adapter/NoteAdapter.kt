package tcd.com.noteapp.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import tcd.com.noteapp.model.entity.Note
import tcd.com.noteapp.model.entity.Tag
import tcd.com.noteapp.ui.adapter.callback.NoteDiffCallback


class NoteAdapter : RecyclerView.Adapter<NoteAdapter.NoteViewHolder>(), Filterable {

    private val mNotes = arrayListOf<Note>()
    private var mShownNotes = arrayListOf<Note>()
    private val mTags = arrayListOf<Tag>()

    private val mNoteFilter = NoteFilter(this, mNotes, mTags)
    var mOnItemClickListener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(tcd.com.noteapp.R.layout.row_note_item, parent, false)
        return NoteViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return mShownNotes.size
    }

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        val note = mShownNotes[position]
        holder.titleTextView.text = note.title
        holder.contentTextView.text = note.content
    }

    fun setNotes(newNotes: List<Note>, query: String? = null) {
        mNotes.clear()
        mNotes.addAll(newNotes)
        if (query.isNullOrBlank()) {
            setNotesInternal(newNotes)
        } else {
            filter.filter(query)
        }
    }

    fun setNotesInternal(newNotes: List<Note>) {
        val diffCallback = NoteDiffCallback(mShownNotes, newNotes)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        mShownNotes.clear()
        mShownNotes.addAll(newNotes)
        diffResult.dispatchUpdatesTo(this)
    }

    fun setTags(newTags: List<Tag>) {
        mTags.clear()
        mTags.addAll(newTags)
    }


    override fun getFilter(): Filter {
        return mNoteFilter.getFilter()
    }

    inner class NoteViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var titleTextView = itemView.findViewById<TextView>(tcd.com.noteapp.R.id.tv_title)!!
        var contentTextView = itemView.findViewById<TextView>(tcd.com.noteapp.R.id.tv_content)!!

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            mOnItemClickListener?.onItemClick(mShownNotes[adapterPosition])
        }
    }

    interface OnItemClickListener {
        fun onItemClick(note: Note)
    }
}