package tcd.com.noteapp.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import tcd.com.noteapp.model.entity.Tag
import tcd.com.noteapp.ui.adapter.callback.TagDiffCallback


class SelectTagAdapter(private var mSelectedTags: HashSet<Int>)
    : RecyclerView.Adapter<SelectTagAdapter.TagViewHolder>() {

    private var mTags = arrayListOf<Tag>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TagViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(tcd.com.noteapp.R.layout.row_tag_selection, parent, false)
        return TagViewHolder(itemView)
    }

    fun setTags(newTags: List<Tag>) {
        val diffCallback = TagDiffCallback(mTags, newTags)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        mTags.clear()
        mTags.addAll(newTags)
        diffResult.dispatchUpdatesTo(this)
    }

    override fun getItemCount(): Int {
        return mTags.size
    }

    override fun onBindViewHolder(holder: TagViewHolder, position: Int) {
        val tag = mTags[position]
        holder.nameTextView.text = tag.name
        holder.selectCheckBox.isChecked = mSelectedTags.contains(tag.id)
    }

    fun getSelectedTag() : ArrayList<Int> {
        return ArrayList(mSelectedTags)
    }

    inner class TagViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var nameTextView = itemView.findViewById<TextView>(tcd.com.noteapp.R.id.tv_title)!!
        var selectCheckBox = itemView.findViewById<CheckBox>(tcd.com.noteapp.R.id.cb_tag_selected)!!

        init {
            itemView.setOnClickListener(this)
            selectCheckBox.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val tagId = mTags[adapterPosition].id
            if (mSelectedTags.contains(tagId)) {
                mSelectedTags.remove(tagId)
                selectCheckBox.isChecked = false
            } else {
                mSelectedTags.add(tagId)
                selectCheckBox.isChecked = true
            }
        }
    }
}