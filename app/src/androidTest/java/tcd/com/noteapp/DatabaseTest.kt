package tcd.com.noteapp

import androidx.test.platform.app.InstrumentationRegistry
import kotlinx.coroutines.runBlocking
import org.junit.After

import org.junit.Before
import tcd.com.noteapp.model.db.AppDatabase
import tcd.com.noteapp.model.db.NoteDao
import tcd.com.noteapp.model.db.TagDao

class DatabaseTest {
    private lateinit var mNoteDao: NoteDao
    private lateinit var mTagDao: TagDao

    @Before
    fun setUp() {
        val database = AppDatabase.getInstance(InstrumentationRegistry.getInstrumentation().targetContext)!!
        mNoteDao = database.noteDao()
        mTagDao = database.tagDao()
        runBlocking {
            mNoteDao.deleteAll()
            mTagDao.deleteAll()
        }
    }

    @After
    fun tearDown() {

    }

//    @Test
//    fun insertNote() = runBlocking {
//        val note = Note(0, "title", "content", listOf(1, 2, 3))
//        mNoteDao.insert(note)
//        val allNotes = mNoteDao.getAll()
//        assertEquals(allNotes.size, 1)
//        note.id = allNotes[0].id
//        assertEquals(note, allNotes[0])
//    }
//
//    @Test
//    fun insertTag() = runBlocking {
//        val tag = Tag(0, "tag")
//        mTagDao.insert(tag)
//        val allTags = mTagDao.getAll()
//        assertEquals(allTags.size, 1)
//        tag.id = allTags[0].id
//        assertEquals(tag, allTags[0])
//    }
}
